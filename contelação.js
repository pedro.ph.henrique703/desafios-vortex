var [tamanho,estrela1,estrela2] = prompt().split(" ").map(s => parseInt(s));
var constelacao = [];
if(tamanho< 4 || tamanho>80){
    console.log("tamanho invalido!");
}else{
    console.log("Constelacao: \n");
    for(let i = 0;i<tamanho;i++){
        let string_ligacao = "[ "
        let ligacao = [];
        let valor = 0
        for(let j = 0; j<tamanho;j++){
            if(j==i){
                valor = 0;
            }else if(i>j){ // se a estrela 0 está conectada com a estrela 2, o contrario também tem que ser verdadeiro;
                valor = (i>0 ? constelacao[j][i]: constelacao[j]); //tratar a matriz constelacão como vetor se tiver na linha 0; 
            }else{
                valor = Math.floor((Math.random()*10)%2); // aleatorizador entre 0 e 1;
            }
            ligacao.push(valor);
            string_ligacao += valor+" ";
        }
        constelacao.push(ligacao);
        console.log(string_ligacao+"]"); // printar a linha das ligações inteira;
    }
    if(constelacao[estrela1][estrela2] == 1){
        console.log("ha ligacao!");
    }else{
        console.log("nao ha ligacao!")
    }
}
