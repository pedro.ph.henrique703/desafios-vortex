var numero_romano = prompt("Digite o número romano");
const romano = { "I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000 };
var numero_decimal = 0;
for (let i = 0; i < numero_romano.length; i++) {
    let valor = 0;
    if (i == numero_romano.length - 1 || romano[numero_romano[i]] >= romano[numero_romano[i + 1]]) {
        valor = romano[numero_romano[i]];
    } else {
        valor = - romano[numero_romano[i]];
    }
    numero_decimal += valor;
}
console.log(numero_decimal);