const possiveis_cartas = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
const possiveis_naipes = ["C", "O", "P", "E"];
const melhores_maos = ["Royal Flush","Straight Flush","Quadra","Full House","Flush","Straight","Trio","Dois Pares","Par","Carta Alta"]

//não existe mesa nesse modo, as cartas da mesa já são posicionadas na mão de cada robo;
var robo1 = [];
var robo2 = [];
var cartasCriadas = [];

//distribuição das cartas;
for (let i = 0; i < 9; i++) {
    while (true) {
        let carta = criarCarta();
        if (!cartasCriadas.includes(carta)) {
            cartasCriadas.push(carta);
            break;
        }
    }
    if (i < 5) {//cartas para os dois(mesa);
        robo1.push(cartasCriadas[i]);
        robo2.push(cartasCriadas[i]);
    } else if (i < 7) {//cartas para o robo1;
        robo1.push(cartasCriadas[i]);
    } else {//cartas para o robo2;
        robo2.push(cartasCriadas[i]);
    }
}

//verificação;
robo1 = sort(robo1);
robo2 = sort(robo2);
var [tipoR1,maoR1] = definir_combinacao(robo1);
var [tipoR2,maoR2] = definir_combinacao(robo2);
console.log("Robô 1: "+tipoR1+" ("+maoR1+")");
console.log("Robô 2: "+tipoR2+" ("+maoR2+")");
if(melhores_maos.indexOf(tipoR1)<melhores_maos.indexOf(tipoR2)){//verifica se a combinação do robo1 é melhor;
    console.log("Robô 1 venceu!");
}else if(melhores_maos.indexOf(tipoR1)>melhores_maos.indexOf(tipoR2)){//verifica se a combinação do robo2 é melhor;
    console.log("Robô 2 venceu!")
}else{//caso as combinações forem de mesmo valor (par,trio,etc.) o programa verifica quem possui a combinação de maior valor;
    let ganhou = 0;// 0: empate, 1: robo1, 2: robo2;
    for(let i = 0; i<maoR1.length;i++){
        if(posicaoCarta(maoR1[i])>posicaoCarta(maoR2[i])){
            ganhou = 1;
            break;
        }else if(posicaoCarta(maoR1[i])<posicaoCarta(maoR2[i])){
            ganhou = 2;
            break;
        }
    }
    switch(ganhou){
        case 0:
            console.log("empate!");
            break;
        case 1:
            console.log("Robô 1 venceu!");
            break;
        case 2:
            console.log("Robô 2 venceu!");
    }
}


//detectar se forma algo;
function definir_combinacao(mao) {
    let combinacao = [];
    let tipo;
    combinacao = sor(mao);
    if (combinacao.length == 5) {
        tipo = combinacao[0][0] == "A" ? "Royal Flush" : "Straight Flush";
        return [tipo, combinacao];
    }
    [tipo, combinacao] = duplicadas(mao);//saber se é quadra;
    if (tipo == "Quadra" || tipo == "Full House") {
        return [tipo, combinacao];
    }
    let combinacao2 = is_flush(mao);
    if (combinacao2.length == 5) {
        tipo = "Flush";
        return [tipo, combinacao2];
    }
    combinacao2 = is_straight(mao);
    if (combinacao2.length == 5) {
        tipo = "Straight";
        combinacao = combinacao2;
    }
    return [tipo, combinacao];
}

function sor(mao) {//straight ou royal;
    for (let i = 0; i <= 3; i++) {
        if (i < 3 || posicaoCarta(mao[i] == 3)) {//posicao 3 = carta com valor 5;
            let flush = [mao[i]];
            let tentativas = 2 - i;
            for (let j = i + 1, f = i; j < mao.length; j++) {// f é a posicao da ultima carta posta no flush;
                if (posicaoCarta(mao[f]) == posicaoCarta(mao[j]) + 1 && naipes_iguais(mao[f], mao[j])) {
                    flush.push(mao[j]);
                    f = j;
                    if (flush.length == 5) {
                        return flush;
                    } else if (flush.length == 4 && posicaoCarta(flush[3] == 0)) {
                        for (let k = 0; k < 3; k++) {
                            if (posicaoCarta(mao[k]) == 12 && naipes_iguais(mao[k], flush[0])) {//exeção de quando o A vale 1;
                                flush.push(mao[k]);
                                return flush;
                            }
                        }
                    }
                } else {
                    tentativas--;
                    if (tentativas < 0) {
                        break;
                    }
                }
            }
        }
    }
    return [];
}

function is_flush(mao) {// detectar se é um flush(normal);
    for (let i = 0; i < 3; i++) {
        let flush = [mao[i]];
        let tentativas = 2 - i;
        for (let j = i + 1, f = i; j < mao.length; j++) {// f é a posicao da ultima carta posta no flush;
            if (naipes_iguais(mao[f], mao[j])) {
                flush.push(mao[j]);
                f = j;
                if (flush.length == 5) {
                    return flush;
                }
            } else {
                tentativas--;
                if (tentativas < 0) {
                    break;
                }
            }
        }
    }
    return [];
}

function is_straight(mao) {//detecta se é um straigh (normal);
    for (let i = 0; i < 3; i++) {
        let flush = [mao[i]];
        let tentativas = 2 - i;
        for (let j = i + 1, f = i; j < mao.length; j++) {// f é a posicao da ultima carta posta no flush;
            if (posicaoCarta(mao[f]) == posicaoCarta(mao[j]) + 1) {
                flush.push(mao[j]);
                f = j;
                if (flush.length == 5) {
                    return flush;
                }
            } else {
                tentativas--;
                if (tentativas < 0) {
                    break;
                }
            }
        }
    }
    return [];
}

function duplicadas(mao) {
    let combinacao = [];
    let tipo = "Carta Alta";
    for (let i = 0; i < mao.length - 1;) {
        let iguais = [mao[i]];
        for (let j = i + 1; j < mao.length; j++) {
            if (posicaoCarta(mao[i]) == posicaoCarta(mao[j])) {
                iguais.push(mao[j]);
            }
        }
        if (iguais.length == 2) {
            switch (combinacao.length) {
                case 0:
                    combinacao = iguais;
                    tipo = "Par";
                    break;
                case 2:
                    combinacao = combinacao.concat(iguais);
                    tipo = "Dois Pares";
                    break;
                case 3:
                    combinacao = combinacao.concat(iguais);
                    tipo = "Full House";
                    break;
            }
        } else if (iguais.length == 3) {
            switch (combinacao.length) {
                case 0:
                    combinacao = iguais.concat(combinacao);
                    tipo = "Trio";
                    break;
                case 2://se houver um par e uma trinca
                    combinacao = iguais.concat(combinacao);
                    tipo = "Full House";
                    break;
                case 3://se houver 2 trincas, reduzimos a trinca de menor vlaor para um par;
                    combinacao = combinacao.concat([iguais[0], iguais[1]]);
                    tipo = "Full House";
                    break;
                case 4://se houver 2 pares e uma trinca, retiramos o par de menor valor;
                    combinacao = iguais.concat([combinacao[0], combinacao[1]]);
                    tipo = "Full House";
                    break;
            }
        } else if (iguais.length == 4) {
            combinacao = iguais;
            tipo = "Quadra";
            break;
        }
        if (tipo == "Full House") {
            break;
        }
        i += iguais.length;//não repetir o loop com carta do mesmo valor;
    }
    for (let i = combinacao.length; i < 5; i++) {//completar a mao caso não for um full house;
        for (let j = 0; j < mao.length; j++) {
            if (!combinacao.includes(mao[j])) {
                combinacao.push(mao[j]);
                break;
            }
        }
    }
    return [tipo, combinacao];
}

function naipes_iguais(carta1, carta2) {
    return carta1[carta1.length - 1] == carta2[carta2.length - 1];
}

function criarCarta() {//cria uma carta aleatória
    let naipe = Math.floor((Math.random() * 10) % 4);
    let valor = Math.floor((Math.random() * 10) % 13);
    return possiveis_cartas[valor] + possiveis_naipes[naipe];
}

// retorna a posicao da carta referente ao dicionario com a sequencia em ordem crescente;
function posicaoCarta(valor) {
    let posicao = possiveis_cartas.indexOf(valor[0]);
    return posicao == -1 ? 8 : posicao; //correção de quando o programa vê o valor da carta sendo 1 em vez de 10;
}

function sort(mao) {//organiza as carta do maior ao menor valor;
    let cartas = Array.from(mao);
    let sorted = [];
    for (let i = cartas.length - 1; i >= 0; i--) {
        let maior = cartas[0];
        for (let j = 0; j < cartas.length; j++) {
            if (posicaoCarta(maior) < posicaoCarta(cartas[j])) {
                maior = cartas[j]
            }
        }
        sorted.push(maior);
        cartas.splice(cartas.indexOf(maior), 1);
    }
    return sorted;
}


